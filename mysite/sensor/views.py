import json

from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render

from sensor.models import Sensordata

# Create your views here.


def index(request):
    latest_data_list = Sensordata.objects.all().order_by('-data_time')[:20]
    context = {'latest_data_list': latest_data_list}
    return render(request, 'sensor/index.html', context)   