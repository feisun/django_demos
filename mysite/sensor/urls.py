from django.conf.urls import patterns, url

from sensor import views

urlpatterns = patterns('',
    # ex: /sensor/
    url(r'^$', views.index, name='index'),
)