from django.db import models

# Create your models here.
class Sensordata(models.Model):
    data_val = models.IntegerField(default=0)
    data_time = models.DateTimeField('time taken')

    def __unicode__(self):              # __unicode__ on Python 2, __str__ on Python 3
        return self.data_val