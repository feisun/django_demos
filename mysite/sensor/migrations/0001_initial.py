# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sensordata',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data_val', models.IntegerField(default=0)),
                ('data_time', models.DateTimeField(verbose_name=b'time taken')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
