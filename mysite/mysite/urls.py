from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
	url(r'^raspberry/', include('raspberry.urls', namespace="raspberry")),
	url(r'^sensor/', include('sensor.urls', namespace="sensor")),
    url(r'^polls/', include('polls.urls', namespace="polls")),
    url(r'^admin/', include(admin.site.urls)),
)