from django.conf.urls import patterns, url

from polls import views

urlpatterns = patterns('',
    # ex: /raspberry/
    url(r'^$', views.index, name='index'),
)