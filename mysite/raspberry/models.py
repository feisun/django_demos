from django.db import models

# Create your models here.
class Sdata(models.Model):
    dval = models.IntegerField(default=0)
    dtime = models.DateTimeField('time taken')

    def __unicode__(self):              # __unicode__ on Python 2, __str__ on Python 3
        return self.dval