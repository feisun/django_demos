# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sdata',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dval', models.IntegerField(default=0)),
                ('dtime', models.DateTimeField(verbose_name=b'time taken')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
